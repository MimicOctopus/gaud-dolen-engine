#include "ImageManager.h"

ImageManager::ImageManager()
{
    //ctor
    hero = new sf::Texture[0];
    setting = new sf::Texture[0];
    npc = new sf::Texture[0];
}

ImageManager::~ImageManager()
{
    //dtor
}




void ImageManager::loadFromFile(std::string type, std::string fileName){
    //do array management stuff
    if(type == "hero"){
        hero = addImage(hero, fileName);
    }
    else if(type == "setting"){
        setting = addImage(setting, fileName);
    }
    else if(type == "npc"){
        npc = addImage(npc, fileName);
    }
}

sf::Texture* ImageManager::addImage(sf::Texture* list, std::string fileName){
    int size = sizeof(list)/sizeof(list[0]);
    sf::Texture* newList= new sf::Texture[size];
    for(int i = 0; i<size; i++){
        newList[i] = list[i];
    }

    //Add exception handling
    newList[size].loadFromFile(fileName);


    return newList;
}

sf::Texture* ImageManager::getHero(){
    return hero;
}
sf::Texture* ImageManager::getSetting(){
    return setting;
}
sf::Texture* ImageManager::getNpc(){
    return npc;
}
