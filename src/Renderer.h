#ifndef RENDERER_H
#define RENDERER_H

#include <SFML/Graphics.hpp>
#include "EventManager.h"
#include "Surface.h"
#include "Unit/PlayableUnit.h"
#include <SFML/System/Clock.hpp>
#include <SFML/System/Time.hpp>

class Renderer : public sf::RenderWindow
{
    public:
        Renderer(sf::VideoMode videoMode, std::string title);
        virtual ~Renderer();
        EventManager* getEvents();
        void render();
        sf::Time getFrameTime();
    protected:
        EventManager* events;
    private:
        sf::Clock clock;
};

#endif // RENDERER_H
