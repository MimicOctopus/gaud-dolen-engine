#ifndef SURFACE_H
#define SURFACE_H

#include <SFML/Graphics.hpp>

class Surface
{
    public:
        Surface(sf::Texture Texture, sf::RenderWindow* App);
        virtual ~Surface();
        void draw();
    protected:

    sf::Texture texture;
    sf::Sprite** sprite;
    sf::RenderWindow* App;

    private:
};

#endif // SURFACE_H
