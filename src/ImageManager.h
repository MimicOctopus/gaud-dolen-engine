#ifndef IMAGEMANAGER_H
#define IMAGEMANAGER_H

#include <SFML/Graphics.hpp>
#include <iostream>

class ImageManager
{
    public:
        ImageManager();
        void loadFromFile(std::string type, std::string fileName);
        virtual ~ImageManager();
        sf::Texture* getHero();
        sf::Texture* getSetting();
        sf::Texture* getNpc();

    protected:
    sf::Texture* hero;
    sf::Texture* setting;
    sf::Texture* npc;

    private:
    sf::Texture* addImage(sf::Texture* list, std::string fileName);

};

#endif // IMAGEMANAGER_H
