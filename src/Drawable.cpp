#include "Drawable.h"

Drawable::Drawable()
{
    //ctor
}

Drawable::~Drawable()
{
    //dtor
}

bool Drawable::isHitting(char direction, Drawable hitable[], sf::Vector2u windowSize){
    if (DUP==direction){
        return (getPosY() > 0);
    } else if (DDOWN==direction){
        return (getPosY() < windowSize.y-64);
    } else if (DLEFT==direction){
        return (getPosX() > 0);
    } else if (DRIGHT==direction){
        return (getPosX() < windowSize.x-48);
    }
    return false;
}

