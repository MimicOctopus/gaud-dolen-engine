#ifndef UNIT_H
#define UNIT_H

#include "../Drawable.h"

class Unit: public Drawable
{
    public:
        Unit();
        virtual ~Unit();
        int getLife();
        int getDef();
        int getAtk();
        void receiveDam(int dam);
        virtual unsigned int getPosX() {return 0;}
        virtual unsigned int getPosY() {return 0;}
    protected:
        //common stats
        int life; //stores the Unit life
        int def; //stores the Unit defense
        int atk; //stores the Unit attack
    private:
};

#endif // UNIT_H
