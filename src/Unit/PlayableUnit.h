#ifndef PLAYABLEUNIT_H
#define PLAYABLEUNIT_H

#include "Unit.h"
#include <SFML/Graphics.hpp>
#include "../EventManager.h"
#include "../Renderer.h"

class Renderer;

class PlayableUnit : public Unit
{
    public:
        PlayableUnit();
        PlayableUnit(int life, int str, int dex, int con, int inte, int wis, int cha, sf::Texture texture, Renderer* window);
        void calcDef();
        void calcAtk();

        void setSpriteSet(sf::Texture spriteSet);
        void draw();
        unsigned int getPosX();
        unsigned int getPosY();
        ~PlayableUnit();
    protected:
        int str;
        int dex;
        int con;
        int inte;
        int wis;
        int cha;
        int armor;
        char direction; //Heading NORTH = 0, SOUTH = 1, WEST = 2, EAST = 3.
        int step;
        int speed;
        EventManager* events;
        Renderer* window;
        sf::Texture spriteSet;
        sf::Sprite sprite;

        //Others
        int wDam; //Temporary weapon damage
    private:
};

#endif // PLAYABLEUNIT_H
