#include "PlayableUnit.h"
#include <iostream>

PlayableUnit::PlayableUnit()
{
    //ctor
}
PlayableUnit::PlayableUnit(int life, int str, int dex, int con, int inte, int wis, int cha, sf::Texture texture, Renderer* window)
{
        this->life = life;
        this->str = str;
        this->dex = dex;
        this->con = con;
        this->inte = inte;
        this->wis = wis;
        this->cha = cha;
        step = 0;
        speed = 100;
        armor = 0;


        this->window = window;
        spriteSet = texture;
        sprite = sf::Sprite(spriteSet);
        sprite.setTextureRect(sf::IntRect(1,1,24,32));
        sprite.setScale(2,2);
        this->events = window->getEvents();
        direction = 0;
        calcAtk();
        calcDef();

}

PlayableUnit::~PlayableUnit()
{
    //dtor
}

void PlayableUnit::calcDef(){
    this->def = this->con + this->armor;
}

void PlayableUnit::calcAtk(){
    this->atk = this->str + this->wDam;
}

void PlayableUnit::setSpriteSet(sf::Texture spriteSet){
    this->spriteSet = spriteSet;
    sprite = sf::Sprite(spriteSet);
}

void PlayableUnit::draw(){
        sf::Time ElapsedTime = window->getFrameTime();
        if(events->getKeyRight() && !events->getKeyLeft()){
            if(isHitting(DRIGHT, NULL, window->getSize()))
                sprite.move(speed * ElapsedTime.asSeconds(), 0);
            sprite.setTextureRect(sf::IntRect(1+(step*24) , 1, 24, 32));
        }
        if(events->getKeyLeft() && !events->getKeyRight()){
            if(isHitting(DLEFT, NULL, window->getSize()))
                sprite.move( - speed * ElapsedTime.asSeconds(), 0);
            sprite.setTextureRect(sf::IntRect(1+(step*24), 97,24,32));
        }
        /*if(events->getKeyUp() && !events->getKeyDown()){
            if(isHitting(DUP, NULL, window->getSize()))
                sprite.move(0, - speed * ElapsedTime.asSeconds());
            sprite.setTextureRect(sf::IntRect(1+(step*24),33,24,32));
        }
        if(events->getKeyDown() && !events->getKeyUp()){
            if(isHitting(DDOWN, NULL, window->getSize()))
                sprite.move(0, speed * ElapsedTime.asSeconds());
            sprite.setTextureRect(sf::IntRect(1+(step*24) , 129 , 24, 32));
        }*/
        if(events->getKeyAttack()){
            //Display current weapon, calculate domage
        }
        window->draw(sprite);
        step++;
        if(step == 8){
            step = 0;
        }
}

unsigned int PlayableUnit::getPosX(){ return sprite.getPosition().x; }
unsigned int PlayableUnit::getPosY(){ return sprite.getPosition().y; }
