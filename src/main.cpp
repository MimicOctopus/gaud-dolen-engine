#include <SFML/Graphics.hpp>
#include "Renderer.h"
#include "Unit/Unit.h"
#include "Unit/PlayableUnit.h"
#include "EventManager.h"
#include "Surface.h"
#include "ImageManager.h"
#include <iostream>


int main()
{
    std::cout<<"Proceding to init"<<std::endl;
    Renderer window(sf::VideoMode(800, 600), "Early Game Stuff");
    window.setFramerateLimit(20);
    std::cout<<"Loading images"<<std::endl;
    ImageManager * images = new ImageManager();
    images->loadFromFile("hero", "walking.tunic.png");
    std::cout<<"Loading images2"<<std::endl;
    sf::Texture * texture = new sf::Texture();

    if (!texture->loadFromFile("tileset0001_tiles.png")){
       std::cout<<"Could not load tiles"<<std::endl;
       return EXIT_FAILURE;
    }

    std::cout<<"Preparing surface"<<std::endl;
    Surface surface(*texture, &window);

    PlayableUnit hero(14, 1, 2, 3, 4, 5, 6, images->getHero()[0], &window);

    std::cout<<"Going in loop"<<std::endl;
    while (window.isOpen())
    {
        window.render();
        surface.draw();
        hero.draw();

        window.display();
    }
}
