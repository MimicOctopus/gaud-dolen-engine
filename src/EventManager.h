#ifndef EVENTMANAGER_H
#define EVENTMANAGER_H

#include <SFML/Graphics.hpp>

class EventManager
{
    public:
        EventManager(sf::RenderWindow * App);
        virtual ~EventManager();
        void ManageEvent();
        bool getKeyRight();
        bool getKeyLeft();
        bool getKeyUp();
        bool getKeyDown();
        bool getKeyQuit();
        bool getKeyAttack();

    protected:
        sf::RenderWindow * App;
        /*bool keyRight;
        bool keyLeft;
        bool keyUp;
        bool keyDown;
        bool keyQuit;*///UNUSED!!!!!
    private:
};

#endif // EVENTMANAGER_H
