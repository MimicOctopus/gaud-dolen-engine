#ifndef DRAWABLE_H
#define DRAWABLE_H

#include <SFML/Graphics.hpp>

#define DUP    0
#define DDOWN  1
#define DLEFT  2
#define DRIGHT 3

class Drawable
{
    public:
        Drawable();
        virtual ~Drawable();
        bool isHitting(char direction, Drawable hitable[], sf::Vector2u windowSize);
        virtual unsigned int getPosX() {return 0;}
        virtual unsigned int getPosY() {return 0;}
        virtual unsigned int getCSizeX() {return 0;}
        virtual unsigned int getCSizeY() {return 0;}
    protected:
    private:
};

#endif // DRAWABLE_H
