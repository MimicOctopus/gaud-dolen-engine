#include "Surface.h"

Surface::Surface(sf::Texture Texture, sf::RenderWindow* App)
{
    //ctor

    this->texture = Texture;
    this->App = App;

    //sprite = new sf:Sprite(Texture)
    //sprite = new sf::Sprite;
    sprite = new sf::Sprite*[32];
    for(int i = 0; i<32; i++){
        sprite[i] = new sf::Sprite[24];
        for(int j = 0; j<24; j++){
        sprite[i][j] = sf::Sprite(texture);
        sprite[i][j].setTextureRect(sf::IntRect(48,24,25,25));
        sprite[i][j].setPosition(i*25, j*25);
        }
    }

}

Surface::~Surface()
{
    //dtor
}

void Surface::draw(){

        for(int i = 0; i<32; i++){
            for(int j = 0; j<24; j++){
                App->draw(sprite[i][j]);
            }
        }
}
