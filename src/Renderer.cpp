#include "Renderer.h"
#include <SFML/System/Clock.hpp>
#include <SFML/System/Time.hpp>
#include <iostream>

Renderer::Renderer(sf::VideoMode videoMode, std::string title)  : sf::RenderWindow(videoMode, title)
{
    //ctor
    events = new EventManager(this);
    //clock;
}

Renderer::~Renderer()
{
    //dtor
}

EventManager* Renderer::getEvents(){
    return events;
}

void Renderer::render(){
        //Manage Key Presses
        getEvents()->ManageEvent();
        //get key code
        if(getEvents()->getKeyQuit()){
            close();
        }
        // Clear screen
        clear();
}

sf::Time Renderer::getFrameTime(){
    return this->clock.restart();
}
